<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="resources/property.pagecontent" var="rb"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:import url="../common/menu.jsp"/>
<html>
<head>
  <title><fmt:message key="message.edit" bundle="${rb}"/> </title>
</head>
<body>
<table class="general_table">
  <caption>
    <fmt:message key="message.headtable.newreservation" bundle="${rb}"/>
  </caption>
<form action="controller" method="post">
  <input type="hidden" name="command" value="add_reservation">
  <tr>
      <td>
        <fmt:message key="message.reservation.datearrival" bundle="${rb}"/>
      </td>
      <td>
        <input type="date" name="arrivaldate" required value="2015-08-29" /><h6>${message}</h6>
      </td>
      <td>
        <fmt:message key="message.reservation.datedeparture" bundle="${rb}"/>
      </td>
      <td>
        <input type="date" name="departuredate" required />
      </td>
    </tr>
    <tr>
      <td><fmt:message key="message.chooselogin" bundle="${rb}"/></td>
      <td>
        <select name="id_room_type">
          <c:forEach var="typeroom" items="${roomtypes}">
            <option>${typeroom.id}</option>
          </c:forEach>
        </select></td>
      <td><input type="submit" value="<fmt:message key="message.button.choose" bundle="${rb}"/>"></td>
      <td class="errormessage">${errorDatePassMessage}</td>
    </tr>
</form>
    <c:forEach var="typeroom" items="${roomtypes}">
      <tr>
        <td>${typeroom.id}</td>
        <td>${typeroom.type.type}</td>
        <td>${typeroom.maxPersons}</td>
        <td>${typeroom.rate}</td>
      </tr>
    </c:forEach>

  </table>
<a href="../../index.jsp">
  <input type="button" id="back_button" value="<fmt:message key="message.button.back" bundle="${rb}"/>">
</a>
</body>
</html>
