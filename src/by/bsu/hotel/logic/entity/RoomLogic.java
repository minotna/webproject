package by.bsu.hotel.logic.entity;

import by.bsu.hotel.dao.exception.ConnectionException;
import by.bsu.hotel.dao.exception.DAOException;
import by.bsu.hotel.dao.reservation.ReservationDAO;
import by.bsu.hotel.dao.room.RoomDAO;
import by.bsu.hotel.entity.reservation.Reservation;
import by.bsu.hotel.entity.reservation.ReservationStatus;
import by.bsu.hotel.entity.room.Room;
import by.bsu.hotel.entity.room.RoomType;
import by.bsu.hotel.logic.exception.LogicException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by 21vek.by on 13.08.2015.
 */
public class RoomLogic {
    public static List<Room> takeAllRooms() throws LogicException {
        try (RoomDAO roomDAO = new RoomDAO()) {
            return roomDAO.takeAllRooms();
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
    }

    public static List<RoomType> takeAllTypes() throws LogicException {
        try (RoomDAO roomDAO = new RoomDAO()) {
            return roomDAO.takeAllTypesRoom();
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
    }

    public static  RoomType takeTypeWithId(int id) throws LogicException {
        try (RoomDAO roomDAO = new RoomDAO()) {
            return roomDAO.takeTypeWithId(id);
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
    }

    public static List<Room>  findFreeRoom(Date arrivalDate, Date departureDate, int maxPersons) throws LogicException {


        List<Room> freeRooms = null;

        try (RoomDAO roomDAO = new RoomDAO()) {
            freeRooms = findRoomsWithMaxPersons(maxPersons);
            ArrayList<Integer> numbers = new ArrayList<>();
            List<Reservation> reservations = new ArrayList<>();
            reservations = ReservationLogic.takeReservationsWithStatus(ReservationStatus.RESERVED);
            int idReservation;
            for (Room room: findRoomsWithMaxPersons(maxPersons)) {
                numbers.add(room.getId());
            }

            for (Integer integer: numbers) {
                boolean checkRoom = true;

                for (Reservation reservation: reservations) {
                    idReservation = reservation.getId();

                    if ((checkRoom)&&(integer.equals(roomDAO.takeRoomWithIdReservation(idReservation).getId()))) {
                        Date dateArrival = reservation.getArrivalDate();
                        Date dateDeparture = reservation.getDepartureDate();
                        boolean a = (arrivalDate.compareTo(dateDeparture)>0);
                        boolean b = (departureDate.compareTo(dateArrival)<0);

                        if(a||b) {

                        } else {
                            Room room  = RoomLogic.findRoomWithId(integer);
                            int  i = freeRooms.indexOf(room);
                            freeRooms.remove(i);
                            checkRoom = false;
                        }
                    }
                }
            }

        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }


        return freeRooms;

    }

    public static Room findRoomWithId(Integer id) throws LogicException {
        Room room = null;
            List<Room> rooms = takeAllRooms();
            for (Room room2: rooms) {
                if (room2.getId()==id) {
                    room = room2;
                }
            }
        return room;
    }

    public static List<Room> findRoomsWithMaxPersons(int maxPersons) throws LogicException {
        List<Room> rooms =new ArrayList<>();

        for (Room room: takeAllRooms()) {
            if(room.getType().getMaxPersons()==maxPersons) {
                rooms.add(room);
            }
        }
        return rooms;
    }

    public static Room takeRoomWithIdReservation(int idReservation) throws LogicException {
        try (RoomDAO roomDAO = new RoomDAO()) {
            return roomDAO.takeRoomWithIdReservation(idReservation);
        } catch (DAOException e) {
            throw new LogicException("Connection problem", e);
        } catch (ConnectionException e) {
            throw new LogicException("DAO problem", e);
        }
    }
}
