package by.bsu.hotel.logic.entity;

import by.bsu.hotel.dao.exception.ConnectionException;
import by.bsu.hotel.dao.exception.DAOException;
import by.bsu.hotel.dao.reservation.ReservationDAO;
import by.bsu.hotel.entity.reservation.Reservation;
import by.bsu.hotel.entity.reservation.ReservationDetail;
import by.bsu.hotel.entity.reservation.ReservationStatus;
import by.bsu.hotel.entity.room.Room;
import by.bsu.hotel.entity.room.RoomStatus;
import by.bsu.hotel.entity.user.UserRole;
import by.bsu.hotel.logic.exception.LogicException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by 21vek.by on 19.08.2015.
 */
public class ReservationLogic {
    public static void writeReservation(int idReservationDetail, Reservation reservation, ReservationStatus status) throws LogicException {
        try (ReservationDAO reservationDAO = new ReservationDAO()) {
            if(!reservation.hasId()) {
                reservationDAO.writeReservation(idReservationDetail,reservation,status);
                int i = reservationDAO.takeLastReservationDetailsId();
                reservation.setId(i);
            }
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }

    }

    public static void writeReservationDetail(ReservationDetail reservationDetail, String login, int idRoomType) throws LogicException {
        try (ReservationDAO reservationDAO = new ReservationDAO()) {
            if(!reservationDetail.hasId()) {
                reservationDAO.writeReservationDetail(reservationDetail, login, idRoomType);
                int i = reservationDAO.takeLastReservationDetailsId();
                reservationDetail.setId(i);
            }
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }

    }

    public static List<Reservation> takeReservationsWithStatus(ReservationStatus status) throws LogicException {
        try (ReservationDAO reservationDAO = new ReservationDAO()){
            return reservationDAO.takeReservationsWithStatus(status);
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
    }

    public static Reservation takeNewReservationWithId(int id) throws LogicException {
        Reservation reservation = null;
        try (ReservationDAO reservationDAO = new ReservationDAO()) {
            List<Reservation> reservations = reservationDAO.takeReservationsWithStatus(ReservationStatus.NEW);
            for(Reservation res: reservations) {
                if(res.getId()==id) {
                    reservation = res;
                }
            }
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
        return reservation;
    }

    public static void submitRoom(int idReservation,int idRoom) throws LogicException {
        try (ReservationDAO reservationDAO = new ReservationDAO()) {
            reservationDAO.submitRoom(idReservation, idRoom);
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }

    }

    public static List<Reservation> takeReservationsWithLogin(String login) throws LogicException {
        try (ReservationDAO reservationDAO = new ReservationDAO()) {
            return reservationDAO.takeReservationsWithLogin(login);
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }

    }

    public static Reservation takeUserReservationWithId(int id, String login) throws LogicException {
        Reservation reservation = null;
        try (ReservationDAO reservationDAO = new ReservationDAO()) {
            List<Reservation> reservations = reservationDAO.takeReservationsWithLogin(login);
            for(Reservation res: reservations) {
                if(res.getId()==id) {
                    reservation = res;
                }
            }
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
        return reservation;
    }

    public static int takeIdStatus(int idReservation) throws LogicException {
        try (ReservationDAO reservationDAO = new ReservationDAO()) {
            return reservationDAO.takeIdStatusReservation(idReservation);
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
    }

    public static void cancelReservation(UserRole role, int id) throws LogicException {
        try (ReservationDAO reservationDAO = new ReservationDAO()) {
            switch (role) {
                case CLIENT:
                    reservationDAO.clientCancelReservation(id);
                    break;
                case ADMINISTRATOR:
                    reservationDAO.adminCancelReservation(id);
                    break;
            }
        } catch (ConnectionException e) {
            throw new LogicException("Connection problem", e);
        } catch (DAOException e) {
            throw new LogicException("DAO problem", e);
        }
    }




}
