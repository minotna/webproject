package by.bsu.hotel.dao.exception;

/**
 * Created by 21vek.by on 11.08.2015.
 */
public class DAOException extends Exception {

    public DAOException() {
    }

    public DAOException(String message) {
        super(message);
    }


    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }


    public DAOException(Throwable cause) {
        super(cause);
    }


    public DAOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
