package by.bsu.hotel.dao.abstractdao;

/**
 * Created by 21vek.by on 11.08.2015.
 */

import by.bsu.hotel.connectionpool.exception.ConnectionPoolException;
import by.bsu.hotel.connectionpool.pool.ConnectionPool;
import by.bsu.hotel.connectionpool.proxyconnection.IConnection;
import by.bsu.hotel.dao.exception.ConnectionException;

/**
 *   Abstract class (AutoCloseable)
 *   Each class in DAO Layer extends it
 *   @see IConnection
 */
public abstract class AbstractDAO implements AutoCloseable{
    protected IConnection connection;

    public AbstractDAO() throws ConnectionException {
        try {
            connection = ConnectionPool.getInstance().takeConnection();
        } catch (ConnectionPoolException e) {
            throw new ConnectionException(e);
        }
    }

    /**
     * return IConnection to ConnectionPool
     */
    public void close(){
        ConnectionPool.getInstance().offerConnection(this.connection);
    }

}
