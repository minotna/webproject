package by.bsu.hotel.dao.room;

import by.bsu.hotel.dao.abstractdao.AbstractDAO;
import by.bsu.hotel.dao.exception.ConnectionException;
import by.bsu.hotel.dao.exception.DAOException;
import by.bsu.hotel.entity.reservation.Reservation;
import by.bsu.hotel.entity.room.AccommodationType;
import by.bsu.hotel.entity.room.Room;
import by.bsu.hotel.entity.room.RoomStatus;
import by.bsu.hotel.entity.room.RoomType;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 21vek.by on 13.08.2015.
 */
public class RoomDAO extends AbstractDAO {
    private final static String ALL_ROOMS = " SELECT rooms.idRoom, roomtypes.idRoomType, accommodationtypes.idAccommodationType, accommodationtypes.type, roomtypes.maxPersons, roomtypes.roomRate \n" +
                                            " FROM rooms \n" +
                                            "JOIN roomtypes ON roomtypes.idRoomType = rooms.roomtypes_idRoomType \n" +
                                            "JOIN roomstatuses ON roomstatuses.idRoomStatus = rooms.roomstatuses_idRoomstatus \n" +
                                            "JOIN accommodationtypes ON accommodationtypes.idAccommodationType = roomtypes.accommodationtypes_idAccommodationType ORDER BY rooms.idRoom";
    private static final String ALL_TYPES_ROOMS = "SELECT roomtypes.idRoomType, roomtypes.roomRate, roomtypes.maxPersons, accommodationtypes.idAccommodationType, accommodationtypes.type \n" +
                                             "FROM roomtypes \n" +
                                             "JOIN accommodationtypes ON accommodationtypes.idAccommodationType = roomtypes.accommodationtypes_idAccommodationType ORDER BY roomtypes.idRoomType";
    private static final String FIND_ROOMTYPE_WITH_ID = "SELECT roomtypes.idRoomType,accommodationtypes.idAccommodationType, accommodationtypes.type, roomtypes.maxPersons, roomtypes.roomRate \n" +
                                                        "FROM roomtypes \n" +
                                                         "JOIN accommodationtypes ON accommodationtypes.idAccommodationType = roomtypes.accommodationtypes_idAccommodationType \n" +
                                                        "WHERE roomtypes.idRoomType=? ";
    private static final String TAKE_ROOM_WITH_IDRESERVATION = "SELECT rooms.idRoom, roomtypes.idRoomType, roomtypes.maxPersons, roomtypes.roomRate, accommodationtypes.idAccommodationType, accommodationtypes.type\n" +
            "FROM rooms\n" +
            "JOIN reservations ON rooms.idRoom = reservations.rooms_idRoom\n" +
            "JOIN roomtypes ON roomtypes.idRoomType = rooms.roomtypes_idRoomType\n" +
            "JOIN accommodationtypes ON accommodationtypes.idAccommodationType = roomtypes.accommodationtypes_idAccommodationType\n" +
            "WHERE reservations.idReservation=? " ;



    public RoomDAO() throws ConnectionException {
    }

    public List<Room> takeAllRooms() throws DAOException {
        List<Room> rooms = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(ALL_ROOMS)) {
            ResultSet resultSet = statement.executeQuery();
            Room room = null;
            while (resultSet.next()) {
                room = new Room();
                room.setId(resultSet.getInt(1));
                RoomType roomType = new RoomType();
                roomType.setId(resultSet.getInt(2));
                AccommodationType accommodationType = new AccommodationType();
                accommodationType.setId(resultSet.getInt(3));
                accommodationType.setType(resultSet.getString(4));
                roomType.setType(accommodationType);
                roomType.setMaxPersons(resultSet.getInt(5));
                roomType.setRate(resultSet.getDouble(6));
                room.setType(roomType);
                rooms.add(room);
            }
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
        return rooms;

    }

    public List<RoomType> takeAllTypesRoom() throws DAOException {
        List<RoomType> types = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(ALL_TYPES_ROOMS)) {
            ResultSet resultSet = statement.executeQuery();
            RoomType roomType = null;
            while (resultSet.next()) {
                roomType = new RoomType();
                AccommodationType accommodationType = new AccommodationType();
                accommodationType.setId(resultSet.getInt(4));
                accommodationType.setType(resultSet.getString(5));
                roomType.setType(accommodationType);
                roomType.setId(resultSet.getInt(1));
                roomType.setRate(resultSet.getDouble(2));
                roomType.setMaxPersons(resultSet.getInt(3));
                types.add(roomType);
            }
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
        return types;
    }

    public RoomType takeTypeWithId(int id) throws DAOException {
        RoomType roomType = new RoomType();
        try (PreparedStatement statement = connection.prepareStatement(FIND_ROOMTYPE_WITH_ID)) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                AccommodationType accommodationType = new AccommodationType();
                accommodationType.setId(resultSet.getInt(2));
                accommodationType.setType(resultSet.getString(3));
                roomType.setType(accommodationType);
                roomType.setId(resultSet.getInt(1));
                roomType.setRate(resultSet.getDouble(5));
                roomType.setMaxPersons(resultSet.getInt(4));
            }
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }
        return roomType;
    }

    public Room takeRoomWithIdReservation(int idReservation) throws DAOException {
        Room room = new Room();
        RoomType  roomType = new RoomType();
        AccommodationType  accommodationType = new AccommodationType();
        try (PreparedStatement statement = connection.prepareStatement(TAKE_ROOM_WITH_IDRESERVATION)) {
            statement.setInt(1, idReservation);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                room.setId(resultSet.getInt(1));
                roomType.setId(resultSet.getInt(2));
                roomType.setMaxPersons(resultSet.getInt(3));
                roomType.setRate(resultSet.getDouble(4));
                accommodationType.setId(resultSet.getInt(5));
                accommodationType.setType(resultSet.getString(6));
                roomType.setType(accommodationType);
                room.setType(roomType);
            }
        } catch (SQLException e) {
            throw new DAOException("SQL problem " + e);
        }

        return room;
    }



}
