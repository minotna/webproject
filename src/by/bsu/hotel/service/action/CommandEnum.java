package by.bsu.hotel.service.action;

import by.bsu.hotel.service.command.admin.*;
import by.bsu.hotel.service.command.client.AddReservationCommand;
import by.bsu.hotel.service.command.client.CreateReservationCommand;
import by.bsu.hotel.service.command.admin.ViewNewReservationsCommand;
import by.bsu.hotel.service.command.client.ReservationDetailCommand;
import by.bsu.hotel.service.command.client.ViewReservationsCommand;
import by.bsu.hotel.service.command.common.*;


/**
 * Created by 21vek.by on 05.08.2015.
 */
public enum CommandEnum {

    AUTH {
        {
            this.command = new AuthorizationCommand();
        }
    },

    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },

    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },

    CHANGE_LANGUAGE {
        {
            this.command = new ChangeLanguageCommand();
        }
    },

    REGISTRATION {
        {
            this.command = new RegistrationCommand();
        }
    },

    NEW_USER {
        {
            this.command = new NewUserCommand();
        }
    },

    NEW_RESERVATIONS {
        {
            this.command = new ViewNewReservationsCommand();
        }
    },

    CREATE_USER {
        {
            this.command = new UserCreateCommand();
        }
    },

    DEL_USER {
        {
            this.command = new UserDeleteCommand();
        }
    },

    DEL_CURRENT_USER {
        {
            this.command = new DeleteUserCommand();
        }
    },

    ALL_ROOMS {
        {
            this.command = new AllRoomsCommand();
        }
    },

    CREATE_RESERVATION {
        {
            this.command = new CreateReservationCommand();
        }
    },

    ADD_RESERVATION {
        {
            this.command = new AddReservationCommand();
        }
    },

    EDIT_RESERVATION {
        {
            this.command = new EditReservationCommand();
        }
    },

    FIND_FREE_ROOM {
        {
            this.command = new FindFreeRoomCommand();
        }
    },

    SUBMIT_ROOM {
        {
            this.command = new SubmitRoomCommand();
        }
    },
    VIEW_RESERVATIONS {
        {
            this.command = new ViewReservationsCommand();
        }
    },

    RESERVATION_DETAIL {
        {
            this.command = new ReservationDetailCommand();
        }
    },

    CANCEL_RESERVATION {
        {
            this.command = new CancelReservationCommand();
        }
    }
    ;

    ActionCommand command;

    public ActionCommand getCurrentCommand() {
        return command;
    }
}
