package by.bsu.hotel.service.manager;

import java.util.ResourceBundle;

/**
 * Created by 21vek.by on 05.08.2015.
 */
public class ConfigurationManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.property.config");
    private ConfigurationManager() { }
    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
