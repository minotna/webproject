package by.bsu.hotel.service.command.common;

import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by 21vek.by on 11.08.2015.
 */
public class RegistrationCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.registration");
        return page;
    }
}
