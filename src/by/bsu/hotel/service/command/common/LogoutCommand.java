package by.bsu.hotel.service.command.common;

import by.bsu.hotel.service.action.ActionCommand;
import by.bsu.hotel.service.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by 21vek.by on 05.08.2015.
 */
public class LogoutCommand implements ActionCommand {
    /**
     * Log out
     * @param request HttpServletRequest
     * @return page address to generate for user
     */
    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().invalidate();
        String page = ConfigurationManager.getProperty("path.page.index");
        return page;
    }
}
