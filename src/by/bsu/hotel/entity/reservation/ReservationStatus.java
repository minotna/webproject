package by.bsu.hotel.entity.reservation;

/**
 * Created by 21vek.by on 16.08.2015.
 */
public enum ReservationStatus {
    NEW, PROCESSING, RESERVED, CANCELED_BY_THE_ADMINISTRATOR, CANCELED_BY_THE_CLIENT, COMPLETED;
}
