package by.bsu.hotel.entity.room;

/**
 * Created by 21vek.by on 05.08.2015.
 */
public enum RoomStatus {
    FREE, BLOCKED, OCCUPIED;
}
