<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="resources/property.pagecontent" var="rb"/>

<c:import url="../common/menu.jsp"/>
<html>
<head>
  <link rel="stylesheet" href="../../css/style.css">

</head>
<body>
<table cellspacing="0" class="general_table">
  <caption><fmt:message key="message.headtable" bundle="${rb}"/></caption>
  <tr align="center">
    <td class="line_with_id">
      <div><fmt:message key="message.pricelist.number" bundle="${rb}"/></div></td>
    <td><fmt:message key="message.pricelist.type" bundle="${rb}"/></td>
    <td><fmt:message key="message.pricelist.rate" bundle="${rb}"/></td>
    <td><fmt:message key="message.pricelist.maxpersons" bundle="${rb}"/></td>
  </tr>
  <c:forEach var="room" items="${rooms}">
    <tr>
      <td>${room.id}</td>
      <td>${room.type.type.type}</td>
      <td>${room.type.rate}</td>
      <td>${room.type.maxPersons}</td>
    </tr>
  </c:forEach>
</table>

<a href="../../index.jsp">
  <input type="submit" id="back_button" value="<fmt:message key="message.button.back" bundle="${rb}"/>">
</a>
</body>
</html>
