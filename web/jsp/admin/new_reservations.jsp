<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="property.pagecontent" var="rb"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:import url="../common/menu.jsp"/>
<html>
<head>
  <title><fmt:message key="message.edit" bundle="${rb}"/> </title>
</head>
<body>
<table class="general_table">
  <tr>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <c:forEach var="reservation" items="${reservations}">
    <form action="controller" method="post">
      <input type="hidden" name="id_reservation" value="${reservation.id}"/>
        <tr>
        <td align="center"><div>${reservation.detail.login}</div></td>
        <td align="center">
          <fmt:formatDate value="${reservation.detail.dateReservation}" type="date"/>
          <fmt:formatDate value="${reservation.detail.dateReservation}" type="time"/>
        </td>
        <td align="center">
          <input type="hidden" name="command" value="edit_reservation">
          <input type="submit" value="Edit"></td>
        </tr>
    </form>

  </c:forEach>
</table>

<a href="../../index.jsp">
  <input type="button" id="back_button" value="<fmt:message key="message.button.back" bundle="${rb}"/>">
</a>
</body>
</html>
