<%--
  Created by IntelliJ IDEA.
  User: 21vek.by
  Date: 11.08.2015
  Time: 19:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="resources.property.pagecontent" var="rb"/>

<c:import url="../common/layout.jsp"/>
<html>
<head>
</head>
<body>
<table align="right">
  <tr>
    <td>
      <a href="controller?command=logout">
        <input type="button" value="<fmt:message key="message.button.logout"  bundle="${rb}"/>">
      </a>
    </td>
  </tr>
</table>
</body>
</html>
